import { combineReducers } from "redux";
import { connectRouter } from 'connected-react-router'

const rootreducer =(history)=>combineReducers({
  router: connectRouter(history),

});
export default rootreducer;