import './matchMedia'
import { render, screen } from '@testing-library/react';
import React from 'react';
import App from './App';
import { Provider } from "react-redux";
import store from "./store";



// let container = null;
// beforeEach(() => {
//   // setup a DOM element as a render target
//   container = document.createElement("div");
//   document.body.appendChild(container);
// });

// afterEach(() => {
//   // cleanup on exiting
//   unmountComponentAtNode(container);
//   container.remove();
//   container = null;
// });

describe("App Test:",()=>{
  it("should show menu burger when in mobile view",()=>{
    window.resizeTo(
      window.screen.availWidth / 2,
      window.screen.availHeight / 2
    );
    const {container}=render(<Provider store={store}><App /></Provider>);
    expect(container.getElementsByClassName('bm-burger-bars').length).toBe(1);


  });
});

