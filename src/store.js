import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import rootReducer from "./reducers";
import { createLogger } from "redux-logger";
import { composeWithDevTools } from "redux-devtools-extension";
import { routerMiddleware } from 'connected-react-router'
import history from "./helpers/history";

const loggerMiddleware = createLogger();
const appRouterMiddleware = routerMiddleware(history);

const composeWithReduxDevtools = composeWithDevTools(
  applyMiddleware(appRouterMiddleware, thunk, loggerMiddleware)
  // other store enhancers if any
);

const store = createStore(rootReducer(history), composeWithReduxDevtools);
export default store;
/**
 * store.js exports the a configured store
 */