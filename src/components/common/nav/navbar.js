import React from "react";
import "./style.scss";
import { stack as Menu } from "react-burger-menu";
import SideBar from "../sidebar";
import {useMediaQuery} from "../../../utils";
const Navbar = (props) => {
  let viewport = useMediaQuery("(max-width: 600px)");

  return (
    <nav className="nav-container">
      <div className="nav-container_left">
        <img src="" alt="logo" />
      </div>
      <div className="nav-container_right">
         {
           viewport?
           null
           :
          props.options.map((e) => {
            return (
              <a
                className="nav-container_right_link"
                href="#"
                onClick={(event) => event.preventDefault()}
              >
                {e}
              </a>
            );
          })
        }
      </div>
    </nav>
  );
};

export default Navbar;
