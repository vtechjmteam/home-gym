import React from "react";
import { stack as Menu } from "react-burger-menu";
import './sidebar.css'
import Logo from '../../../logo.svg'
export default (props) => {
  return (
    // Pass on our props
    <div>
     <img className="mobile-logo" src={Logo} alt="logo"/>
    <Menu {...props}>
      
      <a className="menu-item" href="/">
        Home
      </a>

      <a className="menu-item" href="/burgers">
        About
      </a>

      <a className="menu-item" href="/pizzas">
        Portfolio
      </a>

      <a className="menu-item" href="/desserts">
        Desserts
      </a>
    </Menu>
    </div>
    
  );
};
