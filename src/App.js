
import React from 'react';
import './App.scss';
import Navbar  from'./components/common/nav/navbar';
import { stack as Menu } from "react-burger-menu";
import SideBar from "./components/common/sidebar";
import {useMediaQuery} from "./utils";
import PrivateRoute from './components/PrivateRoute';
import { Route, Switch} from "react-router-dom";
import { ConnectedRouter } from 'connected-react-router';
import history from "./helpers/history";
import Home from './pages/home';
import SignUp from './pages/signup';
import Login from './pages/login';

function App() {
  let viewport = useMediaQuery("(max-width: 600px)");

  const renderRoutes=()=>
 {
  const SWITCH = { ON: true, OFF: false };

  const routes = [
    { path: "/sign-in/", isPrivate: SWITCH.OFF, component: Login},
    { path: "/sign-up/", isPrivate: SWITCH.OFF, component:SignUp},
    // { path: "/dashboard/", isPrivate: SWITCH.OFF, component:DashBoard},
    { path: "/", isPrivate: SWITCH.OFF, component:Home }
    
  ]
  return routes.map((route, index) => {
    
    return route.isPrivate ? (
      <PrivateRoute
        key={index}
        exact
        path={route.path}
        component={route.component}
      />
    ) : (
      <Route key={index} path={route.path} component={route.component} />
    );
  });
}
  const getAppBar =()=>{
    if(viewport){return <SideBar right pageWrapId={'page-wrap'} outerContainerId={'nav-container_right'} />} 
    else{return <Navbar  options={["Home","About","Portfolio"]}/>}  
  }
  return (
    <div className="App" id="outer-container">
    {window.location.pathname==="/"?getAppBar():null}
    
      <div id="page-wrap">
  
      <ConnectedRouter history={history}>
          <div>
            <Switch>{renderRoutes()}</Switch>
          </div>
      </ConnectedRouter>
      </div>
    </div>
  );
}

export default App;
